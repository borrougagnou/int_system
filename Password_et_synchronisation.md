# 01 - Gestionnaire de mot de passe
Keepass est un programme [open-source](https://sourceforge.net/projects/keepass/files/) qui possède un programme de [Bug Bounty](https://www.intigriti.com/public/project/keepass/keepassbyec) et qui est [soutenu financièrement par l'Europe](https://keepass.info/ratings.html)
La base de mot de passe, c'est un seul fichier en `.kbdx`  qui peut être utilisé sur n'importe quel logiciel compatible keepass, ce fichier peut être sécurisé de plusieurs façon:
- mot de passe maitre
- fichier
- TOTP (plugin)
- yubikey (plugin)

le premier lancement est disponible à ce lien (anglais): https://keepass.info/help/base/firststeps.html
et la documentation est disponible dans la partie "Features" à gauche

# 02 - Outils de sauvegarde
J'utilise Syncthing, c'est un projet totalement  [open-source](https://github.com/syncthing/syncthing), multi-plateforme (Windows, Mac, Linux, BSD, Android, Solaris, GTK, ...) crée par la Syncthing fondation et déporté sur plusieurs systèmes

Il possède de nombreux fork et est disponible dans de nombreuse langue.

Sa documentation donne beaucoup d'information sur son fonctionnement: https://docs.syncthing.net/

Facile à comprendre, il permet de synchroniser des dossier et des fichiers en utilisant le P2P avec un chiffrement TLS.

Pour le lancer, on tape `syncthing` dans un terminal / invite de commande (à condition que celui-ci ai bien son path),
et accessible via une page web sur : https://localhost:8384

### Synchronisation
Elle dépend du choix de la configuration (envoie uniquement, réception uniquement, ou les deux)

Si au moins 2 périphériques équipé de Syncthing tournent en même temps et partageant le même dossier, la synchronisation est automatique, on peut voir les fichiers synchronisé en cliquant sur "Changement récent" pour voir ce qui a été synchronisé

### Restauration
Elle dépend du choix de la configuration (Aucune, Mode poubelle, Mode suivi de version, Mode échelonnés, ou Mode gestion externe)

Chaque mode à sa particularité, expliqué quand on sélectionne le mode ou expliqué dans la documentation.

En version échelonné on choisi quel fichier/dossier + version restaurer (ici il n'y a qu'une version et qu'un fichier pour le coup):
![restauration](https://gitlab.com/borrougagnou/int_system/uploads/a9eb914d7bef3e812c2ddd44cab700aa/restauration.PNG)

