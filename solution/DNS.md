# Ajout d'un serveur DNS Primaire et Secondaire
## Présentation

La solution doit comporter un DNS.

## Préparation

- [Le projet](README.md) qui comporte
   - 2 CentOS setup en HAProxy pour la répartition de charge
   - 2 CentOS setup avec NGINX
- 2 CentOS setup avec bind et bind-utils

La configuration IP est en Host-Only, les IPs sont statiques et non en DHCP

- Serveur WEB NGINX:
    - 192.168.48.129
    - 192.168.48.128
- Serveur de LoadBalancing:
    - 192.168.48.130
    - 192.168.48.131
- Serveur de DNS:
    - 192.168.48.134
    - 192.168.48.135

et on va créer un domaine lab.local

On ajoute un hostnames sur les 2 VMs de DNS:
```bash
hostnamectl set-hostname cent7-dns01|dns02
```

on installe les packets `bind` et `bind-utils`
```bash
yum install bind bind-utils
```

## Serveur Primaire
### Configuration
on backup l'ancien le fichier `/etc/named.conf`
et on le remplace par un neuf
```bash
mv /etc/named.conf /etc/named.conf.orig
vim /etc/named.conf
```

et on et ajoute:
```bash
options {
	listen-on port 53 { 127.0.0.1; 192.168.48.134;}; ### DNS primaire ###
	directory     "/var/named";
	dump-file     "/var/named/data/cache_dump.db";
	statistics-file "/var/named/data/named_stats.txt";
	memstatistics-file "/var/named/data/named_mem_stats.txt";
	allow-query { localhost; 192.168.48.0/24;}; ### Reseaux ###
	allow-transfer { localhost; 192.168.48.135; }; ### DNS secondaire  ###
	recursion yes;
	dnssec-enable yes;
	dnssec-validation yes;
	dnssec-lookaside auto;
	bindkeys-file "/etc/named.iscdlv.key";
	managed-keys-directory "/var/named/dynamic";
	pid-file "/run/named/named.pid";
	session-keyfile "/run/named/session.key";
};

logging {
	channel default_debug {
		file "data/named.run";
		severity dynamic;
	};
};

zone "." IN {
	type hint;
	file "named.ca";
};

#DNS transmission
zone "lab.local" IN {
	type master;
	file "forward.lab"; #fichier qu'on devra creer (associer à la variable "directory")
	allow-update { none; };
};

#DNS inverse
zone "48.168.192.in-addr.arpa" IN {
	type master;
	file "reverse.lab"; #fichier qu'on devra creer (associer à la variable "directory")
	allow-update { none; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

ensuite, comme on l'a ajouter dans le fichier sur le DNS inversé, on va le créer:
```bash
vim /var/named/forward.lab
```

et on ajoute:
```bash
$TTL 86400
@   IN  SOA     mdns.lab.local. root.lab.local. (
        2011071001  ;Serial
        3600        ;Refresh
        1800        ;Retry
        604800      ;Expire
        86400       ;Minimum TTL
)
@       IN  NS  mdns.lab.local.
@       IN  NS  sdns.lab.local.
@       IN  A   192.168.48.134
@       IN  A   192.168.48.135

@       IN  A   192.168.48.131
@       IN  A   192.168.48.130
@       IN  A   192.168.48.129
@       IN  A   192.168.48.128

mdns    IN  A   192.168.48.134
sdns    IN  A   192.168.48.135
ha01    IN  A   192.168.48.131
ha02    IN  A   192.168.48.130
nx01    IN  A   192.168.48.129
nx02    IN  A   192.168.48.128
```

mdns et sdns sont des diminutifs de Master DNS et Secondary DNS
ensuite les HA sont les serveurs en HA proxy
et NX sont les serveurs NGINX

et on édit le fichier du reverse DNS:
```bash
vim /var/named/reverse.lab
```

on ajoute:
```bash
$TTL 86400
@   IN  SOA     mdns.lab.local. root.lab.local. (
        2011071001  ;Serial
        3600        ;Refresh
        1800        ;Retry
        604800      ;Expire
        86400       ;Minimum TTL
)
@       IN  NS          mdns.lab.local.
@       IN  NS          sdns.lab.local.
@       IN  PTR         lab.local.

mdns    IN  A   192.168.48.134
sdns    IN  A   192.168.48.135

ha01    IN  A   192.168.48.131
ha02    IN  A   192.168.48.130
nx01    IN  A   192.168.48.129
nx02    IN  A   192.168.48.128

11      IN  PTR         mdns.lab.local.
12      IN  PTR         sdns.lab.local.
13      IN  PTR         ha01.lab.local.
14      IN  PTR         ha02.lab.local.
15      IN  PTR         mx01.lab.local.
16      IN  PTR         mx02.lab.local.
```

on ouvre le port 53 du pare-feu en TCP et UDP et on redémarre le pare-feu
```bash
firewall-cmd --permanent --add-port=53/tcp
firewall-cmd --permanent --add-port=53/udp
firewall-cmd --reload
```

### Check
on va maintenant tester les différentes configurations du fichier
```
named-checkconf /etc/named.conf
named-checkzone lab.local /var/named/forward.lab
named-checkzone lab.local /var/named/reverse.lab
```
le premier ne doit rien renvoyer, par contre les deux autres doivent renvoyer un OK

on démarre les services ensuite:
```bash
systemctl start named
systemctl enable named
```

on doit ensuite ajouter le DNS dans le fichier `/etc/resolv.conf` l'IP du DNS primaire:
```bash
[…]
nameserver 192.168.48.134
```

## Serveur Secondaire
### Configuration
Comme le serveur primaire, on backup le fichier `/etc/named.conf` pour le mettre par un vide

le fichier `/etc/named.conf` est pratiquement identique, on supprime la ligne `allow-transfer` du `option` et modifie cela:
```bash
options {
	listen-on port 53 { 127.0.0.1; 192.168.48.135;}; ### DNS secondaire ###
    [...]
}

[...]

zone "lab.local" IN {
	type slave;
	file "slaves/forward2.lab"; #creation auto d'un fichier "slaves/forward2.lab" dans slave de la variable 'directory'
	masters { 192.168.48.134; };
};

zone "48.168.192.in-addr.arpa" IN {
	type slave;
	file "slaves/reverse2.lab"; #creation auto d'un fichier "slaves/reverse2.lab" dans slave de la variable 'directory'
	masters { 192.168.48.134; };
};

```

il n'y a pas besoin de crée les fichier de transmission et de reverse DNS, ils sont automatiquement transmis du DNS Primaire

pour le reste comme le pare-feu ou le redémarrage des services de DNS c'est la même chose

on doit ensuite ajouter le DNS dans le fichier `/etc/resolv.conf` l'IP du DNS primaire ET du DNS secondaire:
```bash
[…]
nameserver 192.168.48.134
nameserver 192.168.48.135
```

ajouter également sur les autres serveurs (HA01, HA02, NX01, NX02) ces 2 lignes dans le fichier `/etc/resolv.conf`


## Test finaux
On teste avec DIG voir si nous détectons bien les 2 serveurs sur le serveur DNS primaire:

![dig_dns1](/uploads/366c251caed97bc0a3cfd2767164b03a/dig_dns1.png)

----
On vérifie également sur les serveurs:

![dig_ha01-02](/uploads/e0de01a0f1cfcb05b6b0624cd0e5a3df/dig_ha01-02.png)

----
Un petit NSLookup du DNS Secondaire:

![nslookup_dns2](/uploads/ef37114727fcb104e42788bdd0c45c5b/nslookup_dns2.png)

---
et du serveur NX01:

![nslookup_nx01](/uploads/0adac7cd8337996eef35f4143db6bc48/nslookup_nx01.png)


----

Comme on le vois, les 2 serveurs DNS sont opérationnels !
