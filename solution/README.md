# Solution de load balancing

## Présentation
La solution que je dois mettre en place est une solution de Répartisseur de charge
J'utiliserai des VM pour réaliser ce projet

## Préparation

Le script devrait tourner sur 4 VMs de CentOS en version 7 ou 8
 - 2 CentOS setup en HAProxy pour la répartition de charge
 - 2 CentOS setup avec NGINX

La configuration IP est en Host-Only
Serveur WEB NGINX:
 - 192.168.48.129
 - 192.168.48.128

Serveur de LoadBalancing:
 - 192.168.48.130
 - 192.168.48.131

IP Virtuelle:
 - 192.168.48.30

> IL EST EGALEMENT POSSIBLE SI POSSESSION DE VIRTUALBOX ET DE VAGRANT D'UTILISER UNE CONFIGURATION PRÊT-A−L'EMPLOI
ET DE NE PAS FAIRE L'INSTALLATION CI-DESSOUS

Ajout d'un hostname sur chaque VM pour les identifiers :
```bash
hostnamectl set-hostname cent7-XXX
```
XXX correspondant à ha01, ha02, nx01, nx02

## Serveur NGINX
On installe epel-release pour avoir plus de repo sur le gestionnaire YUM, et on installe nginx
### Installation
```bash
yum install epel-release
yum update
yum install nginx
```
On démarre le daemon NGINX
```bash
systemctl enable nginx
systemctl start nginx
systemctl status nginx
```

On ouvre les ports du firewall qui concerne les ports HTTP et HTTPS
```bash
firewall-cmd --zone=public --permanent --add-service=http
firewall-cmd --zone=public --permanent --add-service=https
firewall-cmd –reload
```
![image](/uploads/af0b69d3295d4b2dc2ec67d70aedd2d6/image.png)

### Configuration
On va commencer par identifier nos 2 serveurs NGINX en modifiant le fichier `index.html` sur les 2 serveurs

```bash
vim /usr/share/nginx/html
```
```bash
[...]
<div class="content">
 <h1>SERVEUR 1</h1>
 <p>This page is used to test the proper operation of the
 <strong>nginx</strong> HTTP server after it has been
[...]
```
![image](/uploads/22449b2104b52f9773e935358ec33ac9/image.png)


## Serveur LoadBalancing:
On installe haproxy et keepalived sur les 2 autres serveurs
```bash 
yum -y update
yum -y install haproxy keepalived
```

### HAproxy
On va backup la config initiale de haproxy, et en creer un autre:
```bash
mv /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg.old 
vim /etc/haproxy/haproxy.cfg
```
on utilise cette config sur les 2 VMs:
```bash
global
   # on stock les log du programme tout cours dans "/var/log/syslog"
   log /dev/log local0
   log /dev/log local1 notice
   log /dev/log local6
   # on chroot l'environement
   chroot /var/lib/haproxy
   # on le fait fonctionner qu'avec tel user/groupe
   user haproxy
   group haproxy
   # on le garde en demon
   daemon

defaults
  # on affiche tous les logs
  log global
  # mode de configuration de HAPROXY si il inspecte les msg http superieur du trafic entrant
  mode http
  # option de verbose
  option httplog
  option dontlognull
  timeout connect 5000
  timeout client 50000
  timeout server 50000

#frontend - affiche une interface de statistique de HAProxy
#---------------------------------
frontend http_front
  bind *:80
  stats uri /haproxy?stats
  default_backend http_back

#round robin balancing backend http - reponse au demande
#-----------------------------------
backend http_back
  balance roundrobin

  mode http
  ## adresse des serveurs
  server webserver1 192.168.48.130:80 check    # adresse ip 1er serveur web
  server webserver2 192.168.48.131:80 check    # adresse ip 2em serveur web
```

On vérifie que le serveur est en marche:
![image](/uploads/646cdb3674bd691c62f1be747251a582/image.png)
et on vérifie que la page du frontend s'affiche bien:
![image](/uploads/f27a8923da6fc48a028e4b67826c1d5d/image.png)

### Keepalived
les configurations sont différentes sur les 2 ordinateurs,
dans le fichier /etc/keepalived/keepalived.conf

#### Sur HA01
```bash
vrrp_instance VI_1 {
  state MASTER
  interface ens36 # indiquer le port utilise pour keepalived
  virtual_router_id 68 #meme fonctionnement qu'un VLAN
  priority 200 # la priorite est importante master 200, backup 190 [priorite du master doit etre > priorite du backup] 

  authentication {
    auth_type PASS
    auth_pass p@ssw0rd1v!0l@bl3 #password
  }
  virtual_ipaddress {
    192.168.48.30 # IP Virtuelle
  }
}
```
#### Sur HA02
```bash
vrrp_instance VI_1 {
  state BACKUP
  interface ens36 # indiquer le port utilise pour keepalived
  virtual_router_id 68 #meme fonctionnement qu'un VLAN
  priority 190 # la priorite est importante master 200, backup 190 [priorite du master doit etre > priorite du backup] 

  authentication {
    auth_type PASS
    auth_pass p@ssw0rd1v!0l@bl3 #password
  }
  virtual_ipaddress {
    192.168.48.30 # IP Virtuelle
  }
}
```

on redémarre l'ensemble et on active le tout
```bash
systemctl enable haproxy
systemctl enable keepalived
systemctl start haproxy
systemctl start keepalived
systemctl status haproxy
systemctl status keepalived
```
## Test

Voila ! notre serveur possède un Load Balancing, en haute disponibilité + redondance.

![image](/uploads/d5d20d48e9e47c6abcc41336211ead00/image.png)


un test avec 4 curls sur la même IP nous révèlleras la preuve:
```
curl 192.168.48.30
curl 192.168.48.30
curl 192.168.48.30
curl 192.168.48.30
```

On peut également créer un serveur DNS et ajouter les 2 adresses pour le faire fonctionner


## Backup

Pour ce qui est du backup, on peut utiliser un 3ième serveur qu'on pourra appeler serveur de sauvegarde qui fera des backups continu

une tâche cron qui sauvegarde le site et les configurations (préférer un mode de connexion via certificat et non via password pour éviter tout problème)
```bash
ssh "root@192.168.48.130" "tar zcf - /etc/haproxy > /opt/backup/bkp_48-130_$(date +%Y%m%d%H%M%S).tar.gz" ; ssh "root@192.168.48.129" "tar zcf - /usr/share/nginx > /opt/backup/bkp_48-129_$(date +%Y%m%d%H%M%S).tar.gz"
```

## Restauration
Pour la restauration, on utilise `scp` et on dé-tar le tout
```bash
scp "login@IpServeurBackup:/opt/bkp-finIP_datebkp.tar.gz" .
```

## DNS

Il est possible d'ajouter un [DNS sur le projet](DNS.md)
