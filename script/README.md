# Backup mysqldump script

## Préparation
bien que le script puisse s'executer sans avoir besoin de fichier,
il faut ajouter le fichier `.backupconf` à la racine de votre HOME

## Lancement
```bash
./backup_bdd.sh
```

## Explication du script
### Fonctionnement
Le script est séparé en 4 étapes assez simple à comprendre:
 - variable d'environement et variable système (avec couleur si possible)
 - lecture du fichier avec check et création (ou non) du dossier backup (fonction `read_config_file`)
 - creation de la backup en fonction du nom de la base de donnée (fonction `create_backup`)
 - suppression des versions précédente en fonction des BDD choisies (exemple si la database sélectionné se nomme "config" il ne va supprimer que les X versions précédente du backup "config") (fonction `remove_old`).

### Gestion d'erreur
Pour la gestion d'erreur, le script va procéder de la sorte:
 - Vérification si fichier `.backupconf` existe si non, valeur par défaut
 - Vérification si paramètre `NB_VERSION_MAX` est bien un nombre, si erreur quitte
 - Creation du dossier de la variable `DB_BACKUP_PATH`, si erreur, quitte
 - Lance mysqldump, si celui-ci a une erreur: quitte en supprimant le fichier sql vide/corrompu et renvoie l'erreur

## Restauration
Pour la restauration de la base de donnée:
```bash
mysql -u root -p < db-nom-date.sql
```
