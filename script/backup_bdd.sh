#!/bin/bash

#####################
## Backup script   ##
##                 ##
## ROUGAGNOU Boris ##
## 02.09.2019      ##
#####################

## SYSTEM VAR
LOGFILE='./bkp_mysql.log'
LOGDATE=`date +"%Y-%m-%d %H:%M:%S"`
NOW=`date +"%Y%m%d%H%M%S"`


## DATABASE VAR
DB_BACKUP_PATH='./'
DB_HOST='localhost'
DB_PORT='3306'
DB_USER='root'
DB_PASSWORD=''
DB_NAME=''
NB_VERSION_MAX='5'


### COLOR CONFIG
ESC="\033["
C_RED=$ESC"0;31m"
C_GREEN=$ESC"0;32m"
C_YELLOW=$ESC"0;33m"
C_BWHITE=$ESC"1;37m"
C_BRED=$ESC"1;97;41m"
C_BGREEN=$ESC"1;97;42m"
C_RST=$ESC"m"
###

### ERROR MANAGER
function handle_error()
{
	### PARAM 1 = NUM RETURN
	### PARAM 2 = INFORMATION MESSAGE
	##  PARAM 3 = ERROR MESSAGE
	_result=$1
	_inform=$2
	_error=$3
	if [[ $_result -eq 0 ]]; then
		echo -e "$C_BGREEN [OK] $C_RST $_inform"
		echo "$LOGDATE [OK] > $_inform" >> "$LOGFILE"
	else
		echo -e "$C_BRED [ERROR] $C_RST $_error"
		echo "$LOGDATE [ERROR] > $_error" >> "$LOGFILE"
		exit 1
	fi
	return
}
###



### BEGIN
function read_config_file()
{
	if [[ ! -r $HOME/.backupconf ]]; then
		handle_error 1 "reading \"$HOME\".backupconf" "File \"$HOME/.backupconf\" not exist or permission not granded"
	else
		source "$HOME/.backupconf"
	fi


	handle_error 0 "reading \"$HOME/.backupconf\""


	re='^[0-9]+$'
	if ! [[ $NB_VERSION_MAX =~ $re ]]; then
		handle_error 1 "$NB_VERSION_MAX is a number" "parsing \"$HOME/.backupconf\" : $NB_VERSION_MAX not a number"
	fi

	mkdir -p "$DB_BACKUP_PATH/"
	handle_error $? "create/check \"$DB_BACKUP_PATH\"" "create \"$DB_BACKUP_PATH\", check permission or space"


}

function create_backup()
{
	ERROR_MSG=""
	ERROR_RET=0
	if [[ "$DB_NAME" == "" ]]; then
		ERROR_MSG=` { \
			mysqldump --all-databases --single-transaction \
				  -h ${DB_HOST} \
				  -P ${DB_PORT} \
				  -u ${DB_USER} \
				  -p${DB_PASSWORD} \
				  > "$DB_BACKUP_PATH/db-all-$NOW.sql"; } 2>&1`
		ERROR_RET=$?
		if ! [[ $ERROR_RET -eq 0 ]]; then
			rm "$DB_BACKUP_PATH/db-all-$NOW.sql"
		fi
		handle_error $ERROR_RET "sauvegarde sur \"$DB_BACKUP_PATH/db-all-$NOW.sql\"" "$ERROR_MSG"

	else
		DB_NAME_FORMAT=`echo "$DB_NAME" |tr "|" " "`
		echo "BACKUP $DB_NAME_FORMAT"

		ERROR_MSG=` { \
			mysqldump -h ${DB_HOST} \
				  -P ${DB_PORT} \
				  -u ${DB_USER} \
				  -p${DB_PASSWORD} \
				  --databases $DB_NAME_FORMAT \
				  > "$DB_BACKUP_PATH/db-$DB_NAME-$NOW.sql"; } 2>&1`
		ERROR_RET=$?
		if ! [[ $ERROR_RET -eq 0 ]]; then
			rm "$DB_BACKUP_PATH/db-$DB_NAME-$NOW.sql"
		fi
		handle_error $ERROR_RET "sauvegarde sur \"$DB_BACKUP_PATH/db-$DB_NAME-$NOW.sql\"" "$ERROR_MSG"
	fi
}

function remove_old()
{
	if [[ "$DB_NAME" == "" ]]; then
		echo -e "Suppression des ancien backup `ls -t $DB_BACKUP_PATH | grep ".sql$" | grep "db-all" | awk 'NR>'$NB_VERSION_MAX`  ($NB_VERSION_MAX VERSIONS MAX)"
		RM_FILE=`ls -t $DB_BACKUP_PATH | grep ".sql$" | grep "db-all" | awk 'NR>'$NB_VERSION_MAX | while IFS= read -r f; do
			echo "$DB_BACKUP_PATH/$f"
		done`
	else
		echo -e "Suppression des ancien backup `ls -t $DB_BACKUP_PATH | grep ".sql$" | grep "db-$DB_NAME" | awk 'NR>'$NB_VERSION_MAX`  ($NB_VERSION_MAX VERSIONS MAX)"
		RM_FILE=`ls -t $DB_BACKUP_PATH | grep ".sql$" | grep "db-$DB_NAME" | awk 'NR>'$NB_VERSION_MAX | while IFS= read -r f; do
			echo "$DB_BACKUP_PATH/$f"
		done`
	fi
	rm $RM_FILE 2> "/dev/null"
}

read_config_file
create_backup
remove_old
exit 0
