# int_system

Integration système
Projet de piscine.

## La sécurisation de mot de passe avec synchronisation

 * Les outils que j'utilise sont [Keepass et Syncthing](./Password_et_synchronisation.md)

## Travaux Pratique

 * ![ok](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/OOjs_UI_icon_check-constructive.svg/18px-OOjs_UI_icon_check-constructive.svg.png) Réalisation d'un script de sauvegarde d'une base de données mysql avec [mysqldump](./script)
 * ![ok](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/OOjs_UI_icon_check-constructive.svg/18px-OOjs_UI_icon_check-constructive.svg.png)  Installation de la solution de Load Balancing + Backup-Restaure
 * ![ok](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/OOjs_UI_icon_check-constructive.svg/18px-OOjs_UI_icon_check-constructive.svg.png)  Documentation de la [solution](./solution)
 * ![ok](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/OOjs_UI_icon_check-constructive.svg/18px-OOjs_UI_icon_check-constructive.svg.png)  Ajout de deux [Serveur DNS](./solution/DNS.md)
 * ![ec](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/OOjs_UI_icon_play-ltr-progressive.svg/18px-OOjs_UI_icon_play-ltr-progressive.svg.png)  Ajout d'une solution d'Haute Disponibilité à la solution avec keepalived

